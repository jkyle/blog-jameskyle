#!/usr/bin/env python

import json
import logging
import os

from flask import Flask
from flask import request
from subprocess import PIPE
from subprocess import Popen
from werkzeug.contrib.fixers import ProxyFix


class GitPost(Flask):
    def __init__(self, docroot,
                       logfile="/var/log/gitpost/gitpost.log",
                       debug=False,
                       branch='master'):
        super(GitPost, self).__init__('gitpost')
        self.route("/api", methods=["POST"])(self.parse_request)
        self.docroot = docroot
        self.logfile = logfile
        self.debug = debug
        self.wsgi_app = ProxyFix(self.wsgi_app)
        self.logger.info("Starting gitpost api application")
        self.branch = branch
        self._log_init()

    def _log_init(self):
        if self.debug:
            log_level = logging.DEBUG
        else:
            log_level = logging.INFO

        self.log_format = '%(asctime)s - %(name)s - %(levelname)s - %(message)s'
        self.date_format = "%Y-%m-%d,%H:%M:%S"
        self.logger.setLevel(log_level)
        formatter = logging.Formatter(fmt=self.log_format,
                                      datefmt=self.date_format)
        fh = logging.FileHandler(self.logfile)
        fh.setFormatter(formatter)
        self.logger.addHandler(fh)

    def log_error(self, command, output, error, returncode):
        msg = "Command: {cmd} => {ret}\n-----------------------\n"
        msg += "Output:\n\t{out}\n"
        msg += "Error:\n\t{err}"
        self.logger.error(msg.format(cmd=command,
                                         out=output,
                                         err=error,
                                         ret=returncode))

    def fetch(self):
        self.logger.info("Fetching changes from master")
        self.popen("git fetch origin master")

    def reset(self):
        self.logger.info("Resetting to HEAD")
        self.popen("git reset --hard FETCH_HEAD")

    def clean(self):
        self.logger.info("Cleaning out any cruft")
        self.popen("git clean -df")

    def popen(self, cmd):
        self.logger.debug("Changing into {0}".format(self.docroot))
        self.logger.debug("Executing: {0}".format(" ".join(cmd)))
        os.chdir(self.docroot)
        p = Popen(cmd.split(), stdout=PIPE, stderr=PIPE)
        p.wait()
        out, err = p.communicate()
        if p.returncode != 0:
            self.log_error(cmd, out, err, p.returncode)

    def generate(self):
        self.logger.info("Regenerating blog")
        self.popen("make html")

    def update(self):
        self.logger.info("Updating blog")
        self.fetch()
        self.reset()
        self.clean()
        self.generate()
        self.logger.info("Finished publishing")

    def get_payload(self, request):
        self.logger.info("Parsing payload...")
        payload = json.loads(request.form["payload"])
        self.logger.debug("Received payload: {0}".format(payload))
        return payload

    def get_commits(self, payload):
        commits = payload["commits"]
        self.logger.info("Received commits from branch: {0!r}".format(commits))
        return commits

    def has_branch(self, commits):
        found = False
        for commit in commits:
            branch = commit['branch']
            self.logger.info('Commit was made on branch {0}'.format(branch))
            if branch == self.branch:
                found = True
        return found

    def parse_request(self):
        commits = self.get_commits(self.get_payload(request))
        if self.has_branch(commits):
            self.update()
        return "Received:: {0}".format(request.form)


if __name__ == "__main__":
    debug = True
    app = GitPost(docroot="/var/www/blog.jameskyle.org",
                  logfile="/tmp/gitpost.log",
                  debug=debug)
    app.run()
else:
    app = GitPost(docroot="/var/www/blog.jameskyle.org")
