Zsh Script to Create VMware Fusion 5 Vagrant Box
##################################################
:tags: vmware, vagrant, scripts
:category: miscellaneous
:date: 2013-05-25 09:03:50
:author: James A. Kyle
:slug: zsh-script-to-create-vmware-fusion-5-vagrant-box

I'm sure vagrant_ package will soon support `vmware fusion`_ too, but as of this article
it does not. So I wrote a little script that'll clean up, shrink, and package a 
vmware virtualmachine directory into a vagrant box. 

It's should be run from the directory of the virtual machine. I put some checks
in there to try and verify that the current directory is a vmware vm. However,
if you do run it outside of a vmware vm directory Bad Things May Happen (TM).

Use with caution, I'm not responsible for any unintended consequences, yada 
yada yada.

.. code-block:: bash

        #!/usr/bin/env zsh
        # set: ft=sh
        set -e
        setopt extendedglob

        vmrun stop *.vmx

        # verify we're in a vmware vm directory
        pwd | grep vmwarevm > /dev/null 2>&1

        # just another check, make sure the files in this directory look like vmware 
        # virtual machine files
        ls -d *.(vmxf|nvram|vmsd|vmx|vmdk) > /dev/null 2>&1 

        # remove all files that are not essential for a vagrant box
        rm -f $(ls -d *~*.(vmxf|nvram|vmsd|vmx|vmdk))

        # defragment and shrink the main vmware disk
        vmware-vdiskmanager -d "Virtual Disk.vmdk"
        vmware-vdiskmanager -k "Virtual Disk.vmdk"

        # create the metadata.json for vmware fusion
        cat <<EOF > metadata.json
        {
          "provider":"vmware_fusion"
        }
        EOF

        # box it up using the virtual machine name as the box name.
        box_name=$(basename "${PWD}" .vmwarevm)
        tar cvzf "${box_name}.box" ./*


.. _vagrant: http://www.vagrantup.com/
.. _`vmware fusion`: http://www.vmware.com/products/fusion/overview.html
