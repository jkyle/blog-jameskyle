Disable DHCP Daemon on Default Virtual Nets in VMware Fusion 5
##############################################################
:tags: vmware, virtualization, dhcp
:category: miscellaneous
:date: 2013-05-16 21:21:18
:author: James A. Kyle
:slug: disable-dhcp-daemon-on-default-vmnets-in-vmware-fusion-5

Fusion 5 is a bit different than previous versions. It adds a lot of network 
configuration missing in previous versions. There are two interfaces that
aren't modifiable in the gui; vmnet1 and vmnet8.

These are default networks created by VMware. vmnet1 is VMware's default
hostonly net and vmnet8 is the default nat network. Both have dhcp enabled.

Network configuration options are managed using the vmnet-cfgcli. vmnet-cfgcli
and all VMWare utilities are found in the 
/Applications/VMware Fusion.app/Contents/Library/ directory. To disable dhcp, 
we'd

.. code-block:: bash

    sudo vmnet-cfgcli vnetcfgadd VNET_1_DHCP no
    sudo vmnet-cfgcli vnetcfgadd VNET_8_DHCP no

They basically update configuration values in the 
/Library/Preferences/VMware Fusion/networking file.

Then restart vmware. You can verify the dhcpd service is no longer running by

.. code-block:: bash

    ps aux | grep -i dhcpd | grep vmnet

Zero results means no dhcpd running on VMware's virtual nets.
