Auto-Configure OSX clients for BackupPC
#######################################
:date: 2009-02-20 07:25
:category: administration
:tags: administration, backup, backuppc, osx

This script auto-configures osx 10.4-10.5 clients for backup via
`BackupPC.`_ It creates a hidden backuppc user with standard
permissions. Limits that users sudo permissions to rsync, sets up
public/private key authentication, fliters all incoming ssh connections
using that key to only allow rsync commands.

The project also comes with a Launchd Daemon that starts backuppc at
startup and keeps it alive if anything happens.

`BackupPCOSX`_

.. _BackupPC.: http://backuppc.sourceforge.net/
.. _BackupPCOSX: http://github.com/jameskyle/backuppcosx/tree/master
