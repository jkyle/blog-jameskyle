Agile Project Setup for Python
##############################
:date: 2009-02-20 09:37
:category: programming
:tags: programming, python, virtualenv

This is a quick summarization of the screen casts found at `ShowMeDo.`_.
It's a good quick reference for getting a project up and going with
nosetests, easy\_install, virtualenv, pastescript, and sphinx for
documentation.

This is notÂ intendedÂ to be a zero to go walk through or a cut/paste
example. Depending on particular setup, you may need to install various
modules before hand etc. For information on doing so, refer to beginner
python tutorials and the respective package sites.

.. code-block:: bash

    $ virtualenv mydemo
    $ cd mydemo && source bin/activate
    $ easy_install pastescript
    $ mkdir src && cd src && paster create mypackage

We want our modules available to interactive shells and such so:


.. code-block:: bash

    $ cd mypackage && python setup.py develop

Setting up testing:

.. code-block:: bash

    $ easy_install nose
    $ easy_install coverage

Finally, we install sphinx for professional looking, easy to maintain
documentation.

.. code-block:: bash

    $ easy_install sphinx

Install whatever else you need and you're ready to go.

Many thanks to `Christopher Perkins`_ for putting together the screen
casts that I based this quick reference on.

.. _ShowMeDo.: http://showmedo.com/videos/video?name=2910000&fromSeriesID=291
.. _Christopher Perkins: http://www.percious.com
