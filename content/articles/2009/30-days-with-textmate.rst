30 Days with TextMate
#####################
:date: 2009-06-15 19:32
:category: programming
:tags: osx, programming, textmate

I've been a vim user for years, but since moving to OSX I've been
hearing a lot about TextMate. Since I always encourage others to keep an
open mind and learn new things, I decided to give this editor a solid
shot.
For the next 30 days, I'll TextMate for all my editing, coding, and
general editing needs. I'll keep a tally of pros and cons that I
encounter along the way.

.. table::

  +-----------+--------------------------+--------------------------------------+
  |           | Vim                      | Textmate                             |
  +===========+==========================+======================================+
  | Snippets  | I'm told there's a plugin| Killer feature, highly customizable  |
  |           | called SnipMate, haven't | and flexible \(any scripting language|
  |           | tried it yet though      | you want.\)                          | 
  +-----------+--------------------------+--------------------------------------+
  | Stability | rock solid               | Crashes frequently. Not every 5m or  |
  |           |                          | anything, but about once or twice a  |
  |           |                          | day. \*Seems to be related to a bug  |
  |           |                          | in the help search menu. You can     |
  |           |                          | avoid crashing by not launching      |
  |           |                          | commands through the help search.    |
  +-----------+--------------------------+--------------------------------------+
