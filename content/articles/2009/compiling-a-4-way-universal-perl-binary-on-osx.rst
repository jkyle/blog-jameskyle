Compiling a 4-way universal perl binary on osx
##############################################
:date: 2009-08-12 13:35
:category: programming
:tags: administration, leopard, mac, osx, perl, programming
:slug: 4-way-universal-perl

If you don't like having all of your perl modules overwritten by
cavalier osx server updates, then you're either going with a macports
install or rolling your own. And if you don't want to give up
mod\_auth\_apple, you're probably rolling your own.

If you're using mod\_perl on osx, then you've probably already run into
the issues with the 4-way httpd fat binary and a flat arch mod\_perl not
working. There are various posts around the internet on how to set
archflags to get around this. However, if you're already compiling your
own perl binary the easiest method is to compile it with the flags you
want passed onto your perl modules.

So this is how to compile your perl binary with a 4-way (ppc ppc64
x86\_64 i686) universal arch and by extension, compile all subsequent
modules with the same:

.. code-block:: bash

    export SDK=/Developer/SDKs/MacOSX10.5.sdk/
    ./Configure -Accflags="-arch i686 -arch ppc -arch x86_64 -arch ppc64 \
      -fPIC -nostdinc -B$SDK/usr/include/gcc \
      -B$SDK/usr/lib/gcc -isystem$SDK/usr/include \
      -F$SDK/System/Library/Frameworks" \
      -Aldflags="-arch i686 -arch ppc -arch ppc64 -arch x86_64 \
      -Wl,-syslibroot,$SDK" -de

    make
