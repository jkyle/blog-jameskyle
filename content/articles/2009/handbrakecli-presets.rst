HandBrakeCLI Presets
####################
:date: 2009-02-18 18:38
:category: multimedia
:tags: handbrake, multimedia, presets


Found this over at snipplr, thought I'd duplicate here for my own
reference: AppleTV

.. code-block:: bash

    ./HandBrakeCLI -i DVD -o ~/Movies/movie.mp4 \
      -B 160 -R 48 -E AAC -e x264 -f MP4 \
      -m -p -b 2500 \
      -x bframes=3:ref=1:subme=5:me=umh:no-fast-pskip=1:no-dct-decimate=1:trellis=2

iPod

.. code-block:: bash

    ./HandBrakeCLI -i DVD -o ~/Movies/movie.mp4 -B 160 -R 48 -E AAC \
      -e x264b30 -f MP4 -m -b 1500 \
      -x frameref=1:bframes=0:nofast_pskip:subq=6:partitions=p8x8,p8x4,p4x8,i4x4:qcomp=0:me=umh:nodct_decimate

PS3

.. code-block:: bash

    ./HandBrakeCLI -i DVD -o ~/Movies/movie.mp4 -B 160 -R 48 -E AAC -e x264 -f MP4 -m -p -b 2500 -x level=41

PSP

.. code-block:: bash

    ./HandBrakeCLI -i DVD -o ~/Movies/movie.mp4 -B 128 -R 48 -E AAC -e FFmpeg -f MP4 -m -b 1024


original link `snipplr.com`_

.. _snipplr.com: http://snipplr.com/view/3056/handbrake-cli-presets/
