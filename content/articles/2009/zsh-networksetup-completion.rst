ZSH networksetup completion
###########################
:date: 2009-03-05 19:32
:category: zsh
:tags: completion, zsh, utilities

A zsh completion for the osx networksetup utility. It's not 100% done,
but is mostly working. [`download`_ ]

.. _download: /source/_networksetup
