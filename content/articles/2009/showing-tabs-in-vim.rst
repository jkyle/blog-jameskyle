Showing tabs in Vim
###################
:date: 2009-02-20 15:01
:category: programming
:tags: editors, programming, vim

Now that the lab has decided to use Python, I occasionally run into the
dreaded "indentation error" that is darn near impossible to find by eye.
This is often due to differences in text editor settings, diff'ing two
files, etc.

I've looked for some easy solution on this, the most common retort is
"use a decent editor". Which by decent, they mean whatever you're not
using right now and if you follow the suggestions you end up playing
marry-go-round with all the most popular editors (emacs, vim, textmate,
komodo, Wing, whatever).Â 

Well, this is the best actual solution I've found when it rears its ugly
head. In vim:

.. code-block:: vim

    :syn match TAB_CHAR "\t"

.. code-block:: vim

    :hi link TAB_CHAR Error

After that, you'll see nice highlighted dots showing the indentation
depth. Ugly, but then you can pick out the tabs. I'm sure all the other
excellent editors above also have this ability. :)



