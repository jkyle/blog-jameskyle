*FIX* mod_rewrite: could not init rewrite log lock in child
###########################################################
:date: 2009-02-20 07:47
:category: administration
:tags: administration, apache2, httpd, mod_rewrite
:slug: could-not-init-rewrite-log-lock-fix 

Ran into this issue a couple of times already, duplicating it here so I
dont' have to go dig for it again:

Error:

::

    [crit] (2)No such file or directory: mod_rewrite: could not init rewrite log lock in child

Change This:

.. code-block:: apache2

         RewriteEngine On
         RewriteCond %{REQUEST_METHOD} ^TRACE
         RewriteRule .* - [F]

To this:

.. code-block:: apache2

         RewriteEngine On
         RewriteCond %{REQUEST_METHOD} ^TRACE
         RewriteRule .* - [F]
         RewriteLog /var/log/apache2/rewrite.log
         RewriteLogLevel 0


Original reference is `here`_

.. _here: http://blog.latcarf.com/
