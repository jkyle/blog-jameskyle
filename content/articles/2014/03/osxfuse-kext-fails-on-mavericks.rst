OSXFuse 2.6.2 Kext Fails to Load in Mavericks
##################################################
:tags: osx administration
:category: administration
:date: 2014-03-19 13:33:14
:author: James A. Kyle
:slug: osxfuse-kext-fails-on-mavericks


The Problem
===========

Installing the most recent OSXFuse_ from Homebrew_ (or from the main site),
fails to load the new kext. If you attempt to load the kext, you get errors
in your logs like:

.. code:: bash

    Mar 19 11:34:30 metaverse.local com.apple.kextd[29]: WARNING - \
    Invalid signature -67062 0xFFFFFFFFFFFEFA0A for \
    kext "/Library/Filesystems/osxfusefs.fs/Support/osxfusefs.kext"


The Fix
=======

The workaround is to sign the code yourself. To do so, you'll first need a 
signing certificate. The `Apple Code Signing Guide`_ covers how to do so. If 
you don't have an applie account and have no interest in creating an 
developer certificate, jump to the section called 
"To use the Certificate Assistant to create a self-signed signing identity".

Once you have a certificate, you can sign the kext. 

.. code:: bash

    sudo codesign -s "James Kyle" \
        -f /Library/Filesystems/osxfusefs.fs/Support/osxfusefs.kex

You should replace "James Kyle" with the name you gave the certificate.

After that, all should be well with the world again.


External Links
==============

- OSXFuse_
- Homebrew_
- `Apple Code Signing Guide`_



.. _Homebrew: http://brew.sh
.. _OSXFuse: http://osxfuse.github.io
.. _`Apple Code Signing Guide`: https://developer.apple.com/library/mac/documentation/security/conceptual/CodeSigningGuide/Procedures/Procedures.html#//apple_ref/doc/uid/TP40005929-CH4-SW1
