Manage Docker with Containerized Mesos & Marathon Using Ansible
###############################################################
:tags: administration,devops,mesos,docker,ansible
:category: administration
:date: 2014-08-25 10:46:46
:author: James A. Kyle
:status: draft

Objectives
==========

To round out my quest for a blog adequately dense in tech hipster jargon, this 
article covers the deployment of a Docker_ cluster on bare metal that uses 
Mesos_ & Marathon_ for scheduling. Oh and we'll use Ansible_ to do it. The 
incentive of running Mesos in a container  is portability of the deployment 
method. It reduces the requirements of the underlying host to "any system that 
can run docker".

Requirements
=============

- ansible
- librarian-ansible
- git
- CentOS 7, RHEL 7, Fedora 20+...or whatever
- some servers

Get the Ansible Work Environment
================================

The scripts required for this project are located in my ansible-sandbox_
project. It's a collection of example playbooks for roles I've put together.

:: 

    git clone https://github.com/jameskyle/ansible-sandbox.git
    cd ansible-sandbox
    librarian-ansible install

Since we'll be deploying containerized mesos, you'll need the mesos dockers
I've built.


Subsection
----------

Lorem Ipsum is simply dummy text of the printing and typesetting industry. 
Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, 
when an unknown printer took a galley of type and scrambled it to make a type 
specimen book.

SubSection II
-------------

Lorem Ipsum is simply dummy text of the printing and typesetting industry. 
Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, 
when an unknown printer took a galley of type and scrambled it to make a type 
specimen book.

External Links
==============

- Docker_
- Mesos_
- Marathon_
- Ansible_

.. _Docker: https://www.docker.co://www.docker.com
.. _Mesos: http://mesos.apache.org
.. _Marathon: https://github.com/mesosphere/marathon
.. _Ansible: http://www.ansible.com/home
.. _ansible-sandbox: https://github.com/jameskyle/ansible-sandbox
