Automated Stage3 Gentoo Install Using Ansible
#############################################
:tags: Gentoo,administration,ansible
:category: administration
:date: 2014-08-26 11:58:52
:author: James A. Kyle


Objectives
==========

The objective is to, as far as possible, fully automate a stage3 Gentoo build.
While we're at it, we'd like to add some flexibility in building different
kernel configurations or passing in different parameters like the initial
username, passwords, etc.

Ansible_ fits the bill for this task nicely. 

As a bonus, Ansible is capable of performing this taks on N number of targets
asynchronously. So taking this method and extending it to an environment where
10, 15, or 100 minimal iso's are deployed over pxe and then concurrently
bootstrapping a new Gentoo install should be trivial!

.. note::
    Gentoo is obviously a tweakers OS. This is not meant to be the One True 
    Way (TM) to configure Gentoo. It's rather a demonstration on how you can use
    ansible to configure Gentoo *your way*, automated, and repeatable.

Getting the Roles
=================

First, we'll need to grab the ansible-sandbox_ project that includes an example
playbook using the ansible-gentoo_ role.

.. code:: bash

    % git clone https://github.com/jameskyle/ansible-sandbox.git
    % librarian-ansible install

Preparing the target environment.
=================================

Next, the target environment needs to be prepared. This entails booting to a
Gentoo minimal disk, ensuring that there's networking available, setting a root
password, and enabling ssh. For this tutorial, we'll assume you have a dhcp
server running. If you don't, you'll need to configure networking manually.

.. warning:: 

    When booting in vmware, I found the minimal Gentoo CD often had issues
    getting an ip address on boot. 

.. note::

    The target system can be bare metal or a virtual machine. The kernel 
    included in the ansible-gentoo_ role as the default includes support for 
    VMWare Fusion/Workstation/Desktop v6. 

It also had issues restarting the dhcpcd daemon with /etc/init.d/dhcpcd restart.
I worked around this by issuing the following commands

.. code:: bash

    # kill `ps aux | grep dhcpcd | grep -v grep | awk '{print $2}'`
    # /etc/init.d/dhcpcd restart
    * Stopping DHCP Client Daemon ...
    * Starting DHCP Client Daemon ...

Setting a root password
-----------------------

As with any \*nix, once your logged into the machine it would look something
like this

.. code:: bash

    # passwd
    New password:
    Retype new password:
    passwd: password updated successfully

Enable sshd
-----------

.. code:: bash

    # /etc/init.d/sshd start
    ssh-keygen: generating new host keys: RSA1 RSA DSA ED25519
    * Starting sshd ...

Wipe Any Previous Configurations
--------------------------------

Ansible doesn't perform operations that have already completed. While normally a
good thing, if you had a previous system setup on the target ansible has no way
of knowing if the configurations were from older installs.

I found issues sometimes arose if the partition tables on the main disks had
been wiped, but some lvm volume labels were still lurking around. Wiping
everything ensures a clean build. The main partitions created by these scripts
are

/dev/sda1
    primary 'boot' partition.
/dev/sda2
    primary main partition for volume group
main
    volume group which includes the /dev/sda2 physical volume
/dev/mapper/main-swap
    swap logical volume
/dev/mapper/main-root
    root logical volume

.. code:: bash

    # parted /dev/sda -s -- mklabel gpt
    # lvremove root
    # lvremove  swap
    # vgremove main



Building Out Gentoo
===================

The rest is short and sweet. As automation should be. You'll find an example 
inventory in inventories/Gentoo. Couple of notes, 

- the new system will get its hostname  from the inventory_hostname.
  In the example this is 'too1'. 
- The Gentoo install cd has python 3 as the default, but ansible needs 2.7. So
  make sure your ansible_python_interpretor path is set in your inventory.


.. code:: bash

    % ansible-playbook -i inventories/gentoo \
                       -u root \
                       --ask-pass \
                       playbooks/gentoo.yml

I Don't Like the Way You Configured Gentoo
==========================================

There's a good chance you don't want Gentoo configured the way the role does.
The individual tasks are broken down into components for easy tweaking of each
individual stage of the deploy. You can also drop in your own kernel
configuration or templates, etc. Think of this as a PoC and tweak away!

External Links
==============

- Ansible_
- ansible-sandbox_
- ansible-gentoo_

.. _Ansible: http://www.ansible.com/home
.. _ansible-sandbox: https://github.com/jameskyle/ansible-sandbox
.. _ansible-gentoo: https://github.com/jameskyle/ansible-gentoo
