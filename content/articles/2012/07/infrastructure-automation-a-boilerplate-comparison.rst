Infrastructure Automation: A Boilerplate Comparison
###################################################
:date: 2012-07-08 11:33
:category: miscellaneous
:tags: administration, devops, infrastructure, linux

Introduction
============

We've been using Chef_ to deploy our infrastructure for a while now.
One of the main sticking points we've run into is the barrier to entry
for develoepers as a result of the boiler plate required to perform a
simple deployment task. Our ideal framework would be equally accessible
to the ops, who use the system every day, and development teams, who
know their application environments best.

In that spirit, this article compares the boilerplate necessary to
perform a simple configuraiton task. Since one of the most common, and
straight forward, needs is installing a system package we'll use that as
an example. This is not intended to demonstrate that one framework is
overall "better" than another. Rather it's merely a comparison of
workflows.

Contenders
==========

The following frameworks are covered:

-  Chef_
-  Puppet_

Article Series Format
=====================

I already have a chef environment ready to go. So first, we'll cover
that workflow. In subsequent posts, I'll cover the other frameworks.

The Objective
=============

To install the tmux package.

The Chef Workflow
=================

For the purposes of our example, we assume `Chef Server`_ is already
configured and working. We also assume a working repository is already
created and the users credentials/environment is set up.

Step One: Create a Cookbook
---------------------------

First, we create the chef cookbook.

::

    % knife cookbook create tmux 
    ** Creating cookbook tmux 
    ** Creating README for cookbook: tmux 
    ** Creating metadata for cookbook: tmux

Step Two: Edit the Default Recipe
---------------------------------

Next, we edit the package to perform the task we need. To do so, we open
the default recipe.

::

    % vim cookbooks/tmux/recipes/default.rb 

And add the following:

.. code-block:: ruby

    # add multiple case statements for other target platforms 
    case node[:platform]  
      when "ubuntu","debian"   
        package "tmux" do     
          action :install   
        end 
      end

Though technically not required, updating the metadata is certainly best
practice.

::

    % vim cookbooks/tmux/metadata.rb

.. code-block:: ruby

    maintainer       "AT&T Foundry" 
    maintainer_email "email@domain.com" 
    license          "All rights reserved" 
    description      "Installs/Configures tmux" 
    long_description IO.read(File.join(File.dirname(__FILE__), 'README.md')) 
    version          "0.0.1"


Step Three: Add the Cookbook to Your Repository
-----------------------------------------------

::

    % git add cookbooks/tmux 
    % git commit cookbooks/tmux -m "Tmux package cookbook added" 
    % git pull origin master 
    % git push origin master

Step Four: Upload Cookbook to Chef Server
-----------------------------------------

::

    % knife cookbook upload tmux 
    Uploading tmux             
    [0.0.1] upload complete

Step Five: Add Recipe to Role
-----------------------------

Next we need to add the recipe to a role. This may require the creation
of a new role. In that case, an additional creation, commit, upload is
required for the role.

::

    % vim roles/tmux.json

.. code-block:: json

    {   
      "name": "tmux",   
      "default_attributes": {   },   
      "json_class": "Chef::Role",   
      "env_run_lists": {   },   
      "run_list": [     
        "recipe[tmux]"   
      ],   
      "description": "Install Tmux",   
      "chef_type": "role",   
      "override_attributes": {   } 
    }

Commit & Upload the role:

::

    % knife role from file roles/tmux.json 
    % git commit roles/tmux.json -m "added tmux role" 
    % git push origin master

Step Six: Apply Role to Server
------------------------------

The final step is applying the role to a server.

::

    knife bootstrap 10.4.54.9 -i ~/.ssh/keys/jk328n_rsa -d ubuntu12.04-gems-att \
      -x ubuntu --sudo -N jkyle-test -E foundry-services_pao1 -r 'role[tmux]'

Followup Articles
=================

-  `Puppet Boiler Plate`_

.. _Chef: http://opscode.com
.. _Puppet: http://puppetlabs.com/
.. _Ansible: http://ansible.github.com/
.. _`Salt Stack`: http://saltstack.org/
.. _`Chef Server`: http://wiki.opscode.com/display/chef/Chef+Server
.. _`Puppet Boiler Plate`: /2012/07/puppet-boilerplate/
