A Boilerplate Comparison: Puppet
################################
:date: 2012-07-15 15:20
:category: miscellaneous
:tags: administration, chef, devops, infrastructure, linux, puppet
:slug: puppet-boilerplate

Introduction
============

Here we cover the steps for deploying a package to a puppet agent/node.
Like with chef, we assume a working installation.

Though I must take the opportunity to comment that the installation
process for puppet was ridiculously easy.

The ridiculously easy way
=========================

Package installation can be performed completely through the console,
e.g. web ui, for puppet. To do so, you go to https://puppetmaster.me.com
and click 'Live Management' -> Advanced Tasks -> package tasks ->
Install. Type in the package name you wish to install and click 'Run'.

That's it. Done.

The not quite ridiculously easy way
===================================

That seems almost too trivial. It's nice that the trivial tasks are
trivial to perform, but let's take a closer look at what it takes to
roll your own task and deploy it. It's more representative of how a
sysadmin will interact with the framework and seems more fair to chef.

Creating a module from scratch
------------------------------

Puppet, like chef, can generate a module template. Puppet is very
community centric and sharing of modules is the default. As such, the
module template asks you to prepend your Puppet Forge username as it is
the best practice naming convention for shared modules. Like in our
previous walk through with chef, we're going to write a package for
installing tmux.

Create the package
~~~~~~~~~~~~~~~~~~

.. code-block:: bash

    % puppet module generate username-tmux

You're rewarded with a directory like

.. code-block:: bash

    Modulefile README     manifests  spec       tests

Edit Modulefile
~~~~~~~~~~~~~~~

You should always edit the modulefile and enter pertinent information
such as version, license, description, etc.

Create tmux class
~~~~~~~~~~~~~~~~~

The business is in the init.pp file, we edit that and add our package
requirement.

.. code-block:: ruby

    class tmux { package{"tmux": } }

Add class to a node
~~~~~~~~~~~~~~~~~~~

Finally, to apply the role to a node you add it to the nodes definition
in the site.pp file. This isn't a hard, fast rule. Puppet can pull node
configuration from external sources. Puppet Enterprise console provides
this and so you could use the web frontend to add the class to your list
in console and then apply it to a group. You could also provide your own
backend.

However, the "basic" basic, out the box puppet involves editing the
site.pp, so we&squot;ll add it to the default node here

.. code-block:: ruby

    node default {
      class {'tmux':}
    }

We also commit the package to our repository.

.. code-block:: bash

    % git add modules/username-tmux manifests/site.pp
    % git commit modules/username-tmux manifests/site.pp -m "added tmux module"
    % git push origin master

Comments
--------

If you noticed, we didn't cover how you upload your module and manifest
like we did in the `Chef`_ article. That's because puppet doesn't have a
management tool for interacting with the puppet master like the knife
tool. Deployment is left up to the administrator. Some common methods
are using cap deploy, commit hooks, or pulling files down in a cron from
the repo.

`Previous Article`_

.. _Chef: /2012/07/infrastructure-automation-a-boilerplate-comparison/
.. _Previous Article: /2012/07/infrastructure-automation-a-boilerplate-comparison/
