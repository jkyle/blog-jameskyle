Adding Syntactic Sugar to Python dicts
######################################
:date: 2012-01-28 16:44
:category: miscellaneous
:tags: programming, python, recipes

Snippet
=======

This little snippet allows dict keys to be accessed like attributes. For
example

.. code-block:: python

    d = CustomDict({"foo": "bar", "bah": {"baz": "bot"}})

Can be accessed like

.. code-block:: python

    d.foo # returns "bar"
    d.foo.bah.baz # returns "bot"

In addition to the hash key lookups.

.. code-block:: python

    class CustomDict(dict):
        def __init__(self, data):
            d = data.copy()
            self.__dict__ = self
            for key, value in data.iteritems():
                if isinstance(value, dict):
                    d[key] = CustomDict(value)
                    map(self.__setitem__, d.keys(), d.values())


