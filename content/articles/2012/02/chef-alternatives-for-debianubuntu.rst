Chef Alternatives for Debian/Ubuntu
###################################
:date: 2012-02-05 10:32
:category: linux
:tags: administration, debian, linux, ubuntu
:slug: chef-alternatives-for-debian-ubuntu

An update-alternatives script for chef. Currently only sets up a version
installed in the 1.9.1 gems directory, but extendable to any version
easily enough.

.. code-block:: bash

    #!/bin/bash
    RUBY_VERSION=1.9.1
    CHEF_VERSION=0.10.8
    GEM_ROOT=/var/lib/gems/${RUBY_VERSION}/gems/chef-${CHEF_VERSION} 
    update-alternatives \
       --install /usr/bin/chef-client chef ${GEM_ROOT}/bin/chef-client 500 \
       --slave /usr/bin/chef-solo chef-solo ${GEM_ROOT}/bin/chef-solo \
       --slave /usr/bin/knife knife ${GEM_ROOT}/bin/knife \
       --slave /usr/bin/shef shef  ${GEM_ROOT}/bin/shef
