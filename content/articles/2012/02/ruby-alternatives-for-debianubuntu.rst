Ruby Alternatives for Debian/Ubuntu
###################################
:date: 2012-02-05 10:14
:category: debian
:tags: administration, debian, linux

A quick update-alternatives script for ruby versions on debian based
systems. It only includes the ruby binaries, man pages, etc. included in
the ruby1.8 and ruby1.9.1 debs. It's defaults to '1.9' when in auto
mode.

After running the script, you can switch between ruby version via

::

    % update-alternatives --config ruby

.. code-block:: bash

    #!/bin/bash
    RUBY_VERSION=1.9.1
    update-alternatives \
       --install /usr/bin/ruby ruby /usr/bin/ruby${RUBY_VERSION} 500 \
       --slave /usr/bin/erb erb /usr/bin/erb${RUBY_VERSION} \
       --slave /usr/bin/rake rake /usr/bin/rake${RUBY_VERSION} \
       --slave /usr/bin/irb irb /usr/bin/irb${RUBY_VERSION} \
       --slave /usr/bin/gem gem /usr/bin/gem${RUBY_VERSION} \
       --slave /usr/bin/rdoc rdoc /usr/bin/rdoc${RUBY_VERSION} \
       --slave /usr/bin/testrb testrb /usr/bin/testrb${RUBY_VERSION} \
       --slave /usr/share/man/man1/rdoc.1.gz rdoc.1.gz \
           /usr/share/man/man1/rdoc${RUBY_VERSION}.1.gz \
       --slave /usr/share/man/man1/testrb.1.gz testrb.1.gz \
           /usr/share/man/man1/testrb${RUBY_VERSION}.1.gz \
       --slave /usr/share/man/man1/rake.1.gz rake.1.gz \
           /usr/share/man/man1/rake${RUBY_VERSION}.1.gz \
       --slave /usr/share/man/man1/irb.1.gz irb.1.gz \
           /usr/share/man/man1/irb${RUBY_VERSION}.1.gz \
       --slave /usr/share/man/man1/erb.1.gz erb.1.gz \
           /usr/share/man/man1/erb${RUBY_VERSION}.1.gz \
       --slave /usr/share/man/man1/gem.1.gz gem.1.gz \
           /usr/share/man/man1/gem${RUBY_VERSION}.1.gz \
       --slave /usr/share/man/man1/ruby.1.gz ruby.1.gz \
               /usr/share/man/man1/ruby${RUBY_VERSION}.1.gz \
       --slave /usr/share/menu/ruby ruby_menu /usr/share/menu/ruby${RUBY_VERSION} 
       
       RUBY_VERSION=1.8
       update-alternatives \
       --install /usr/bin/ruby ruby /usr/bin/ruby${RUBY_VERSION} 400 \
       --slave /usr/bin/erb erb /usr/bin/erb${RUBY_VERSION} \
       --slave /usr/bin/irb irb /usr/bin/irb${RUBY_VERSION} \
       --slave /usr/bin/rdoc rdoc /usr/bin/rdoc${RUBY_VERSION} \
       --slave /usr/bin/testrb testrb /usr/bin/testrb${RUBY_VERSION} \
       --slave /usr/share/man/man1/rdoc.1.gz rdoc.1.gz \
           /usr/share/man/man1/rdoc${RUBY_VERSION}.1.gz \
       --slave /usr/share/man/man1/testrb.1.gz testrb.1.gz \
           /usr/share/man/man1/testrb${RUBY_VERSION}.1.gz \
       --slave /usr/share/man/man1/irb.1.gz irb.1.gz \
           /usr/share/man/man1/irb${RUBY_VERSION}.1.gz \
       --slave /usr/share/man/man1/erb.1.gz erb.1.gz \
           /usr/share/man/man1/erb${RUBY_VERSION}.1.gz \
       --slave /usr/share/man/man1/ruby.1.gz ruby.1.gz \
           /usr/share/man/man1/ruby${RUBY_VERSION}.1.gz \
       --slave /usr/share/menu/ruby ruby_menu \
           /usr/share/menu/ruby${RUBY_VERSION} 
