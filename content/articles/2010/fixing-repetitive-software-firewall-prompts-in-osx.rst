Fixing repetitive software firewall prompts in OSX
##################################################
:date: 2010-09-07 07:47
:category: osx
:tags: administration, osx, utilities
:slug: fix-firewall-prompts-osx

Sometimes the OS X software firewall gets stuck in a loop and will
continuously prompt to allow network access for an application.

.. figure:: http://blog.jameskyle.org/wp-content/uploads/2010/09/49034307.jpg
   :align: center
   :alt: 49034307.jpg

   49034307.jpg
 
I've seen this happen a couple of times with iTunes. Though it can
happen with any application. A common cause is changing icons of system
applications.

The fix is pretty easy though. First check if the application's code
signature is valid and if not, why:

::

    % codesign -vvv /Applications/iTunes.app # or the problem application
    /Applications/iTunes.app: a sealed resource is missing or invalid
    /Applications/iTunes.app/Contents/Resources/English.lproj/DevicePrefsNotes.nib/objects.xib: resource added
    /Applications/iTunes.app/Contents/Resources/English.lproj/MobilePhonePrefs.nib/objects.xib: resource added
    /Applications/iTunes.app/Contents/Resources/English.lproj/PartyShuffleSettings.nib/objects.xib: resource added
    /Applications/iTunes.app/Contents/Resources/English.lproj/UniversalAccess.nib/objects.xib: resource added

We see it fails the signing and there are some extraneous objetcs.xib
files. The error indicates these files weren't included in the original
signing of the application, they should be safe to delete. Even if we
break the application, we can always reinstall after cleaning up.

After deleting each of the offending files we get a very different
output:

::

    % codesign -vvv /Applications/iTunes.app
    /Applications/iTunes.app: valid on disk
    /Applications/iTunes.app: satisfies its Designated Requirement

No more prompting!
