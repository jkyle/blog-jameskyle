Creating a redmine email filter for postfix with virtual domains
################################################################
:date: 2010-06-12 12:47
:category: administration
:tags: administration, email filter, postfix, redmine, virtual domains
:slug: postfix-virtual-domain-redmine-filter

This is adapted from the postfix virtual redme for `autoreply
transports`_. As such, I basically copy/paste that tutorial and edited
it accordingly.

This assumes that you have set up redmine at its own subdomain as well
as a postfix virtual domain, e.g. redmine.yourdomain.com and wish to
create a virtual redmine user for email processing, e.g.
redmine@redmine.yourdomain.com.

In order to set up an redmine for virtual recipients as a final stop (no
more filters applied), set up a rule in a virtual alias table:

.. code-block:: text

    /etc/postfix/main.cf:
        virtual_alias_maps = hash:/etc/postfix/virtual
    /etc/postfix/virtual:
        redmine@redmine.yourdomain.com redmine@redmine.yourdomain.com@yourdomain.com



This delivers mail sent to redmine@redmine.yourdomain.com to the local
address that handles the email. We service the address locally into the
script parses emails for redmine.


DO NOT list redmine.yourdomain.com in mydestination!



.. code-block:: text

    /etc/postfix/main.cf:
        transport_maps = hash:/etc/postfix/transport
    /etc/postfix/transport:
        redmine.yourdomain.com  redmine

.. code-block:: text

    /etc/postfix/master.cf:
    redmine unix  -       n       n       -       10      pipe
        user=nobody argv=/path/to/redmine/extra/mail_handler/rdm-mailhandler.rb
        --url http://redmine.yourdomain.com --key [redmine api key] --project kspecs
        --allow-override project,priority

This invokes/path/to/redmine/extra/mail\_handler/rdm-mailhandler.rb and
pipes any email sent to the redmine domain. Redmine handles rejection of
invalid users, which is why we don’t bother with filtering valid
addresses within postfix.

After this is done, do the standard postmap routine for your maps, then
postfix reload.

.. _autoreply transports: http://www.postfix.org/VIRTUAL_README.html#autoreplies
