Dell Perc 5i on Ubuntu 64
#########################
:date: 2010-01-19 08:38
:category: administration
:tags: administration, dell, perc5i, ubuntu, ubuntu64

I've been setting up an Ubuntu server on a Dell with a 29xx series with
a Perc 5i SAS/SATA RAID controller. It took a bit of digging around and
piecing together posts on the ubuntu forums and random blogs, but I'm
pretty sure I've got it all up and working. Here's the short story:

#. Add the deb ports for dell's Open Management tools to your
   sources.conf

   #. /etc/apt/sources.list: deb `ftp://ftp.sara.nl/pub/sara-omsa`_ dell
      sara
   #. add the sara key to your gpg list

      #. wget `http://ftp.sara.nl/debian\_sara.asc`_
      #. sudo apt-key add debian\_sara.asc

   #. apt-get update

#. Install snmp tools

   #. apt-get install snmp snmpd

#. Install openipmi and ipmitool

   #. apt-get install openipmi
   #. apt-get install ipmitool
   #. there's an open Ubuntu bug `#110992`_ that requires ipmi\_devinf
      and ipmi\_si to be appended to /dev/modules by hand to enable
      creation of the /dev/ipm\* needed by ipmitool on start-up.

#. Install lib32 ncurses and ia32-libs

#. 

   #. apt-get install -f lib32ncurses5
   #. apt-get install -f ia32-libs

#. Install dell's omsa tools

   #. apt-get install dellomsa

#. Update your libraries

   #. ldconfig

   #. 

      #. this takes care of a few errors I got that looked like this
      #. /opt/dell/srvadmin/dataeng/bin/dsm\_sa\_datamgr32d: error while
         loading shared libraries: libdcsmil32.so.5: cannot open shared
         object file: No such file or directory

#. Place the following init script and put it in /etc/init.d/

   #. .. code-block:: bash

          #!/bin/sh

          /opt/dell/srvadmin/dataeng/bin/dsm_sa_datamgr32d
          /opt/dell/srvadmin/dataeng/bin/dsm_sa_eventmgr32d
          /opt/dell/srvadmin/dataeng/bin/dsm_sa_snmp32d

          /opt/dell/srvadmin/dataeng/bin/dataeng status

          /opt/dell/srvadmin/iws/bin/linux/dsm_om_connsvc start 
          /opt/dell/srvadmin/iws/bin/linux/dsm_om_connsvc start 

          /etc/init.d/dataeng stop ; modprobe mptctl ; /etc/init.d/dataeng start

          /opt/dell/srvadmin/dataeng/bin/dsm_sa_datamgr32d
          /opt/dell/srvadmin/dataeng/bin/dsm_sa_eventmgr32d
          /opt/dell/srvadmin/dataeng/bin/dsm_sa_snmp32d

          /opt/dell/srvadmin/dataeng/bin/dataeng status

          /opt/dell/srvadmin/iws/bin/linux/dsm_om_connsvc start 
          /opt/dell/srvadmin/iws/bin/linux/dsm_om_connsvc start 

#. Enable snmp in omsa tools

   #. /etc/init.d/dataeng enablesnmp

#. Execute the dell\_omsa.sh script to get up and running

   #. /etc/init.d/dell\_omsa.sh

#. Register the new driver module with omsa

   #. /etc/init.d/instsvcdrv restart

#. Edit your /etc/snmp/snmpd.conf so it can be used (I take no
   responsibility if the following settings are not appropriate security
   for your network)

   #. change: com2sec paranoid default public to com2sec readonly
      default public

#. Change the way taht ubuntu starts snmpd

   #. /etc/default/snmpd: change
   #. SNMPDOPTS='-Lsd -Lf /dev/null -u snmp -I -smux -p
      /var/run/snmpd.pid 127.0.0.1' to
   #. SNMPDOPTS='-Lsd -Lf /dev/null -u snmp -p /var/run/snmpd.pid
      127.0.0.1'

#. /etc/init.d/snmpd restart
#. /etc/init.d/dataeng restart
#. Verify snmpd

   #. snmpwalk -OS -v 1 -c public localhost .1.3.6.1.4.1.674.10892.1

#. Start OMSA web services on reboot

   #. update-rc.d dsm\_om\_connsvc defaults

#. Finally, by default you must log into the web admin with the root
   account. So enable root account

   #. sudo passwd root

#. One more catch for 64 bit ubuntu users, you must change the lib paths
   in /etc/pam.d/omauth and install 32 bit pam libs

   #. change all instances of /lib/security/ to /lib32/security
   #. download the i386 versions of the following libs (you can
      determine the package that provides a particular lib by dpkg -S
      /path/to/file, e.g. dpkg -S /lib/security/pam\_unix.so)

      #. libselinux1\_2.0.15-2ubuntu1\_i386.deb
      #. libpam-modules\_0.99.7.1-5ubuntu1\_i386.deb
      #. libsepol1\_2.0.3-1\_i386.deb

   #. Extract them to a tmp directory

      #. dpkg-deb -x <libname> /tmp/lib32s

   #. then copy the following over to /lib32 and /lib32/security

      #. lib/libsepol.so.1
      #. lib/libselinux.so.1
      #. lib/security/pam\_unix.so
      #. lib/security/pam\_nologin.so

   #. run ldconfig

#. You should now be able to log into the OMSA web manager at
   `https://localhost`_:1311 using your root name password
#. Having root enabled can be viewed as a security list, so you probably
   want to add a normal admin user to OMSA's manager group and disable
   root

   #. adduser dellroot
   #. usermod -g root dellroot # user must be added to the root group to
      gain admin privileges in OMSA manager

I'd like to thank the `sad software blogger`_ for a good bit of this
information, especially pertaining to snmp.

.. _`ftp://ftp.sara.nl/pub/sara-omsa`: ftp://ftp.sara.nl/pub/sara-omsa
.. _`http://ftp.sara.nl/debian\_sara.asc`: http://ftp.sara.nl/debian_sara.asc
.. _#110992: https://bugs.launchpad.net/ubuntu/+source/ipmitool/+bug/110992
.. _`https://localhost`: https://localhost
.. _sad software blogger: http://sadsoftware.blogspot.com/2008/08/installing-dell-omsa-and-snmp-in-ubuntu.html
