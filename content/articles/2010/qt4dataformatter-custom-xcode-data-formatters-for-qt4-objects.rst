Qt4DataFormatter (custom Xcode data formatters for Qt4 objects)
###############################################################
:date: 2010-05-23 12:43
:category: programming
:tags: data formatters, programming, qt4, xcode
:slug: xcode-qt4-data-formatter

I do most of my development on OS X, including Qt4 projects. Qt4 objects
are particularly averse to inspection in the Xcode debugger.
Fortunately, Xcode has a `data formatter plugin API`_ for displaying
summaries of  user defined objects. Unfortunately, the documentation
isn't as complete as one would hope. The example program doesn't even
compile. Through a bit of trial and error, I was able to create data
formatters for some basic Qt4 objects. Included are:

-  QString
-  QVariant
-  QModelIndex
-  QFile
-  QDomNode
-  QDomElement

I'm adding other objects on an as needed basis. Adding other QObjects is
trivial using the existing methods as a template. Basically, you just
create a display message using a QString and standard Qt4 API's, then
have the new data formatter function return the already implemented
printQString(QString\* str, int ID) method. For example, if we wished to
create a summary for QObject's that displayed the QObject's classname,
we would add a method to Qt4DataFormatter.cpp in the form of:

.. code-block:: objc

    char* printQObject(QObject *obj, int ID)
    {
      QString result("ClassName: %1");
      QString className("Undefined");

      // make sure we have access to the plugin function list
      if (NULL != _pbxgdb_plugin_functions ) {
        className = ogj->metaObject()->className();
      }

      return printQString(result.arg(className), ID);
    }

Recompile, then install (or link) the plugin to your:

.. code-block:: bash

    ~/Library/Application\ Support/Developer/Shared/Xcode/CustomDataViews/

or 

.. code-block:: bash

    /Developer/Library/Xcode/CustomDataViews/

It's that easy. The `Qt4DataFormatter project`_ is hosted on github.
Please see the README for the project as well.


.. _data formatter plugin API: http://developer.apple.com/mac/library/documentation/DeveloperTools/Conceptual/XcodeDebugging/220-Viewing_Variables_and_Memory/variables_and_memory.html
.. _Qt4DataFormatter project: http://github.com/jameskyle/Qt4DataFormatter
