Customized Qt4 Generated Xcode Projects
#######################################
:date: 2010-05-18 15:52
:category: programming
:tags: programming, qt4, xcode


One of the very nice features of Qt's qmake is the ability to generate
platform specific IDE projects. However, unless I've missed something in
the Qt docs customizing the build settings for generated projects is
very limited. Since often run qmake to update other generated files
(ui\_, moc\_, etc), I had to reconfigure Xcode each time. To get around
this, I wrote a Python script that that first runs qmake with -spec
macx-xcode and then modifies the generated Xcode project with custom
settings. The custom settings themselves are defined in a `YAML`_ file.
The script expects to be run from the Qt project directory and expects
to find a file called xcode.yml in that directory that contains your
custom build options. My xcode.yml:

.. code-block:: yaml

    pbxproject:
      compatibilityVersion: Xcode 3.2
    Debug:
      ONLY_ACTIVE_ARCH: "YES"
      SDKROOT: /Developer/SDKs/MacOSX10.6.sdk
      ARCHS: $(ARCHS_STANDARD_32_64_BIT)
      VALID_ARCHS: "i386 x86_64"
      GCC_WARN_EFFECTIVE_CPLUSPLUS_VIOLATIONS: "YES"
    Release:
      ONLY_ACTIVE_ARCH: "NO"
      VALID_ARCHS: "i386 x86_64 ppc ppc64"

The environment variables and their values are a 1-to-1 for what you see
in Xcode's Build pane of Project Info if you right click on a field and
select "Show Definitions" and "Show Settings Names". The source can be
downloaded `here`_.


.. _YAML: http://www.yaml.org/
.. _here: http://github.com/jameskyle/Qt4-Xcode-Config
