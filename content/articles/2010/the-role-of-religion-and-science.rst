The role of religion and science
################################
:date: 2010-12-02 10:52
:category: miscellaneous
:tags: personal beliefs, philosophy, religion, science

Greg Fish has a pretty interesting `article`_ on the religious victim
complex. The comments sparked a good discussion on the roles of religion
and science.

In particular, the proposition was made that "S*cience answers ‘How?’;
religion answers ‘Why?’".*\ I took contention with this statement,
proposing that science is perfectly adequate at answering 'why'
questions. Below is a response to that assertion by `Bruce Coulson`_, my
reply got quite verbose so I thought it deserved its own blog
entry. I'll walk through the main points one by one.

    "Religion provides meaning. Science doesn’t."

I take umbrage at this. The natural conclusion would be that those who
do not have religion have no meaning in their lives. This is
demonstrably false. All the atheists I know, including myself, have very
clearly defined concepts of the "meaning" of their lives. I also know
many a religious person who is extremely conflicted over what the
meaning of their lives are. Now, that's purely anecdotal (most of the
atheists I know are academic or business professionals and very goal
oriented) and not meant to reflect the normal population as a whole. But
if I just produce a single atheist that has meaning, that negates the
assertion that it can \_only\_ be found through religion or
spirituality.

    It does not answer, and cannot answer, ‘Why?’ If you counter with,
    ‘well, evolution is the function of natural laws’ then the counter
    is ‘Why do the natural laws work this way?’ Ultimately, your answer
    (from a scientific point of view) boils down to “Because they do!”
    Which is perfectly valid and correct, scientifically. But
    unsatisfying from a faith point of view, because it’s not an answer.

I'm not sure what's being implied here. When a child asks "Why do
oranges fall to the ground?" replying "Gravity" is a perfectly valid
response.

Pointing out that science is not infinite in its ability to answer the
"Why" questions is not a nail in its coffin. Also, "Because they do." is
never, ever a valid answer from the perspective of science. However, "We
don't know." is. This is a key difference between Religion and science.
Religion insists that when traversing the ladder of Why's there must be
turtles all the way down. Science does not require that every observed
phenomena have a definitive answer. Though lack of an answer is
certainly an indication that there is still interesting work to do.

Science views unanswered questions as areas to be investigated, not
areas resolved arbitrarily by a god which need no further investigation.
Not only is it ok to say "I don't know." in science, it's a requirement
before you can even begin to embark on a scientific inquiry. It is
religion that is perfectly fine with show stopping answers to why such
as "Because god made it that way."

To delve a little deeper into this issue and get at what I think the
core contention is in this "Why" analogy, there are some questions which
are *unanswerable*. They are posited in such a way that they are
untestable. For example, if you define "spirit" as an entity that does
not exist on this plane of reality and is therefore undetectable by
anything in this reality. Or the framing of morality in a way that does
not entail any sort of claimed effect on our lives. The problem with
these sorts of questions is that `the moment you remove a proposed
"thing" from our reality`_, it no longer impacts this reality (by
definition). The "answer" to an untestable question can be any one of an
infinite set of arbitrary "solutions". Since they are all equally
untestable, they are all equally valid (or invalid) with maybe a loose
requirement of logical consistency. I say "loose" because except in the
works of the most academic of religious philosophers such as
`Plantinga`_, `Alston`_, and others this requirement is never followed.
In fact, the biggest issue that rigorous modern philosophers of religion
have is trying to shoe horn religion into some sort of logically
consistent, rational framework with varying degrees of success depending
on whether you're speaking to a religiously affiliated philosopher or
not.

To drive this home, claiming that X is moral means nothing if there is
no proposed positive or negative effect of doing X. It simply doesn't
impact our lives at all. As soon as you make some claim, such as "If
people X, then it improves Y" or even something as vague as "If people
X, they will feel better." you've thrown yourself into the realm of
science as we can certainly devise a means of testing that hypothesis.

To claim that religion is only concerned with those questions that are
unanswerable not by some limitation of our instruments, but by the
inherent nature of the question, is to assert that religion is only
concerned with those questions which do not impact our lives. I think
this is a proposition that even the most academic of religious
philosophers would reject.

.. _article: http://worldofweirdthings.com/2010/12/01/your-morning-dose-of-would-be-martyrdom/
.. _Bruce Coulson: http://worldofweirdthings.com/2010/12/01/your-morning-dose-of-would-be-martyrdom/#comment-32422
.. _the moment you remove a proposed "thing" from our reality: http://godlessgeeks.com/LINKS/Dragon.htm
.. _Plantinga: http://en.wikipedia.org/wiki/Alvin_Plantinga
.. _Alston: http://en.wikipedia.org/wiki/Alvin_Plantinga
