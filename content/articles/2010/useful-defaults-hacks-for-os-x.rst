Useful Defaults Hacks for OS X
##############################
:date: 2010-08-21 09:35
:category: osx
:tags: administration, mac, osx, tips, utilities
:slug: osx-defaults-hacks

-  Disable Guest Accounts for Shares on OSX Tiger

   ::

       sudo defaults write /Library/Preferences/com.apple.AppleFileServer guestAccess -bool false

-  Disable Dashboard

   ::

       defaults write com.apple.dashboard mcx-disabled -boolean YES

-  Reduce Safari Rendering delay

   ::

       defaults write com.apple.Safari WebKitInitialTimedLayoutDelay 0.25

-  Increase animation speed of sheets

   ::

       defaults write NSGlobalDomain NSWindowResizeTime .001

-  Disable spotlight

   ::

       edit /etc/hostconfig to change "SPOTLIGHT=-YES-" to "SPOTLIGHT=-NO-"

-  Enable Safari debug menu

   ::

       defaults write com.apple.Safari IncludeDebugMenu 1

-  Get rid of .DS\_Store files on network shares (specifically works for
   smb shares)

   ::

       defaults write com.apple.desktopservices DSDontWriteNetworkStores true

-  Show full path to current directory in Finder title

   ::

       defaults write com.apple.finder _FXShowPosixPathInTitle -bool YES

-  Mount external drives at boot rather then on user login

   ::

       defaults write /Library/Preferences/SystemConfiguration/autodiskmount 

   AutomountDisksWithoutUserLogin -bool true

-  Setting a custom software update server for a client

   ::

       defaults write /Library/Preferences/com.apple.SoftwareUpdate CatalogURL "http://customupdateserver.com/index.sucatalog"

-  Show All Files in Finder

   ::

       defaults write com.apple.finder AppleShowAllFiles TRUE

-  Terminal Focus Follows Mouse

   ::

       defaults write com.apple.Terminal FocusFollowsMouse -string YES

-  Add Application to OSX Dock (example uses Calculator.app)

   ::

       defaults write com.apple.dock persistent-apps -array-add "< dict>tile-data file-data_CFURLString /Applications/Calculator.app/_CFURLStringType0 ";

-  Disable the OSX Crash Reporter

   ::

       defaults write com.apple.CrashReporter DialogType none

-  Add Recent Applications Stack to the Dock

   ::

       defaults write com.apple.dock persistent-others -array-add '{ "tile-data" = { "list-type" = 1; }; "tile-type" = "recents-tile"; }'

-  Change Login Screen Background

   ::

       sudo defaults write /Library/Preferences/com.apple.loginwindow DesktopPicture "/Library/Desktop Pictures/Aqua Blue.jpg"

-  Disable Time Machine External Disk as Backup Prompt

   ::

       defaults write com.apple.TimeMachine DoNotOfferNewDisksForBackup -bool YES

-  Disable mouse/trackpad acceleration

   ::

       defaults write .GlobalPreferences com.apple.mouse.scaling -1

   ::

       defaults write .GlobalPreferences com.apple.trackpad.scaling -1
