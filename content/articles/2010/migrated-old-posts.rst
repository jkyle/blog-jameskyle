Migrated old Posts
##################
:date: 2010-06-12 11:57
:category: miscellaneous

I migrated my old blog posts from `CCN`_ into this one via rss. It looks
like it mostly worked, formatting is a little off so my apologies for
that. I'll try to touch them up "as needed".

.. _CCN: http://ccn.ucla.edu/users/jkyle
