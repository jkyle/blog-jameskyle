kindle 3 review (my first ereader)
##################################
:date: 2010-09-07 14:21
:category: reviews
:tags: books, devices, ebooks, reviews, kindle

.. image:: http://g-ecx.images-amazon.com/images/G/01/kindle/shasta/photos/big-viewer-3G-01-lrg._V188696038_.jpg
    :scale: 25%
    :width: 50%
    :alt: Kindle 3G
    :align: center

Introduction
~~~~~~~~~~~~

I just received my Kindle 3 in the mail. I decided to purchase a Kindle
for a few reasons. The first is a larger screen than the iPhone and the
second is better low light reading. The glare from the iPhone screen
would get to me after a while, but only in low light. On the flip side,
in areas of very bright light the reflective glare makes the iPhone text
difficult to read.

I thought of the iPad but rejected it because of the glare issue and it
was much more than I wanted. I have a laptop, a desktop, and iPhone. I
don't really need to add "really big iPhone" to that. When you add the
extra $400'sh to the price tag of the iPad over the Kindle and it seemed
the logical choice.

First Impressions
~~~~~~~~~~~~~~~~~

Packaging
^^^^^^^^^

It's definitely small and thin. About 1/2 the thickness of an iPhone (or
less) and much thinner than an iPad. The Kindle feels lighter, so I was
surprised to find it weighed 75g more than the iPhone on my scale. I
assume because the weight is distributed over a larger area. I weighed a
couple of 300 page paperbacks off my shelf and the Kindle seems to
approximate an average weight for that size book. In short, it's very
comfortable in to hold. Not too heavy, but not feather light either.
Another important size metric for me is whether it'll fit in my pocket.
In today's device driven world I need something that I can tuck away
without too much trouble. The Kindle fit neatly  in my back pocket, no
problem. Even less problem than a folded up paperback or magazine.

Interface
^^^^^^^^^

The interface is well designed and about as intuitive as can be. It took only a matter of seconds before I had my first book open and my settings configured. My only complaint here is the placement of the next/previous side buttons. They're just a tiny bit too low for my thumb to comfortable rest by them when holding the Kindle naturally. I'd prefer the buttons be slightly higher on the device. 

.. figure:: /images/2010/09/Holding-Kindle.jpg
   :align: center
   :alt: Holding-Kindle.jpg

Readability
^^^^^^^^^^^

The text on this is jaw dropping clear. You can see from the picture
above that even black & white images pop. The promises of e-ink are
certainly delivered. And better yet, the text becomes *more*\ clear with
greater light and not less.

Final Words
~~~~~~~~~~~

That's it for first impressions. I'll revisit this post after a couple
of weeks of use and becoming more comfortable with the device to update
any changes to my first impressions or make additions.
