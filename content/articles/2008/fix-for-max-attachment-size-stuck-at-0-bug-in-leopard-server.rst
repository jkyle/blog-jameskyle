Fix for max attachment size stuck at 0 bug in leopard server
############################################################
:date: 2008-08-21 15:36
:category: administration
:tags: administration, apple, leopard, mac, osx

There's a bug in Server Admin which makes the max attachment size stick
at 0 MB preventing file uploads. To fix this:

-  Manually set your preferred attachment size in /etc/wikid/wikid.conf
   under the *maxattachmentsize*\ key
-  Save file
-  restart the team server on the command line

   .. code-block:: bash

       sudo serveradmin stop teams
       sudo serveradmin start teams    
