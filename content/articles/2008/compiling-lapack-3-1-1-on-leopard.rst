Compiling lapack 3.1.1 on Leopard
#################################
:date: 2008-08-23 08:07
:category: programming
:tags: apple, leopard, mac, osx, programming
:slug: compiling-lapack-on-leopard

OSX contains an incomplete lapack library by default in the vecLib
framework. But if you want the full power of lapack 3.1.1, you have to
compile from source. This article documents that process, mostly so when
I have to do it again I'll have a reference.

The first thing we notice is that OSX does not contain a fortran
compiler in its developer tools. The `gfortran project`_  contains a
nice dmg installer. Two other options are compiling from source or using
macports. I chose macports.

.. code-block:: bash

    $ sudo port install gcc43

Create some convenient links for standard gfortran calls.

.. code-block:: bash

    $ sudo ln -sf /opt/local/bin/gfortran-mp-4.3 /usr/local/bin/gfortran
    $ sudo ln -sf /opt/local/bin/gfortran-mp-4.3 /usr/local/bin/g77     

Next download `lapack.tgz`_ source (current 3.1.1 as of this writing)
and untar it.

.. code-block:: bash

    $ tar xzvf lapack.tgz

Lapack does not contain the standard configure script, so we have to
edit the make.inc ourselves. Copy the make.inc.example to make.inc, then
apply `this patch`_. 

.. code-block:: bash

    $ patch < make.inc.patch

That's it, all that's left is to compile. I do so with 6 make threads.

.. code-block:: bash

    $ make -j6

.. _gfortran project: http://gcc.gnu.org/wiki/GFortran"
.. _lapack.tgz: http://www.netlib.org/lapack/lapack.tgz
.. _this patch: /source/2008/08/make.inc.patch
