Creating postfix aliases
########################
:date: 2008-09-04 17:49
:category: administration
:tags: mail, mtu, postalias, postfix



Just a reminder of the postfix alias flow. I don't do it enough to remember 
off the top of my head and this saves me the google time.



1. edit /etc/aliases or /etc/postfix/aliases (one is a symlink to the
other):

.. code-block:: bash

    # postalias /etc/aliases
    # newaliases
    # postfix reload
