Leopard Server Postfix TLS Error: cannot get private key from file /etc/certificates/mycert.key
###############################################################################################
:date: 2008-09-09 07:28
:category: administration
:tags: apple, leopard, mac, osx
:slug: leopard-server-encrypted-tls-fix

The problem here is that postfix is failing on encrypted TLS certs and
OSX ServerAdmin created certs are encrypted. The fix is, as should be
expected, to unencrypt the cert:

.. code-block:: bash

    cd /etc/certificates
    cp mycert.key mycert.key.saved
    openssl rsa -in mycert.key -out mycert.key.out
    cp -p mycert.key.out mycert.key
    postfix reload

* credit to the apple discussion forums.




