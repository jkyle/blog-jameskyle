Disable Extended Attributes for Tar Backup
##########################################
:date: 2008-10-09 11:06
:category: administration
:tags: administration, extended attributes, osx, tar

To disable the tar'ing of extended attributes (those pesky .\_foo
files), export the following variable:

For Tiger:

.. code-block:: bash

    export COPY_EXTENDED_ATTRIBUTES_DISABLE=true

For Leopard & Snow Leopard:

.. code-block:: bash

    export COPYFILE_DISABLE=true




