Fixing errant Share Points
##########################
:date: 2008-09-19 07:30
:category: administration
:tags: dscl, ldap, mac, osx, share points

Recently, I ran into an issue where Share Points which were removed from
Server Admin were showing up as stale links in my clients. Though not
deal breaking, it was annoying as it indicating some sort of corruption
in my ldap configuration. 

After digging around, I found that the records still existed in my
server's LDAP under /Mounts. Including a duplicate entry for my
/Network/Applications entry. By removing these and reloading the clients
automount (sudo automount), everything went back to normal.
