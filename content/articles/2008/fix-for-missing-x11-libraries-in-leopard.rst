Fix for missing X11 libraries in Leopard
########################################
:date: 2008-09-04 17:43
:category: administration
:tags: apple, leopard, mac, osx, x11



This solution comes straight from: `Penny Smalls.`_. I've just put it
here for personal reference.

::

    dyld: NSLinkModule() error
    dyld: Library not loaded: /usr/X11R6/lib/libdpstk.1.dylib
      Referenced from: /usr/local/lib/ruby/gems/1.8/gems/rmagick-1.15.9/lib/RMagick.bundle
      Reason: image not found
    Trace/BPT trap

Those dylib files do not exist any more. But… I have a backup (I hope
you do). I ended up doing this:

.. code-block:: bash

    cd /Volumes/BackupDisk/usr/X11R6/lib
    sudo cp libdps*1.0* /usr/X11R6/lib/
    sudo ln -s /usr/X11R6/lib/libdpstk.1.0.dylib /usr/X11R6/lib/libdpstk.1.dylib
    sudo ln -s /usr/X11R6/lib/libdps.1.0.dylib /usr/X11R6/lib/libdps.1.dylib

.. _Penny Smalls.: http://pennysmalls.com/2007/10/28/breakage-in-leopard-rmagick-fails-to-find-libdpstk1dylib/
