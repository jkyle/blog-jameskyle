Command Line Group Management
#############################
:date: 2008-12-16 13:53
:category: administration
:tags: administration, dscl, osx

Adding a user:

.. code-block:: bash

    dscl . append /Groups/admin GroupMembership gneagle

Removing a user:

.. code-block:: bash

    dscl . delete /Groups/admin GroupMembership gneagle

Reading the membership of the admin group:

.. code-block:: bash

    dscl . read /Groups/admin GroupMembership

This comes with a nod to `Managing OSX`_

.. _Managing OSX: http://managingosx.wordpress.com/2006/09/15/add-a-user-to-the-admin-group-via-command-line-20/
