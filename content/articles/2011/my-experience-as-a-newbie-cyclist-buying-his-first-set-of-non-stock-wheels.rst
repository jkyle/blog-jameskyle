My experience as a newbie cyclist buying his first set of non stock wheels.
###########################################################################
:date: 2011-05-05 08:59
:category: reviews
:tags: cycling, fitness, wheels, reviews

So, I've been rocking stock Open Sport's for about 10 or 11 thousand
kilometers and they've about had it. Couple of good dings and not really
holding true anymore. Time for some new wheels.

The Contenders
--------------

I looked at a few commercial wheel options and at four wheel builders:

-  `Bicycle Wheel Warehouse`_
-  `Predator Cycling`_
-  `Wheel Builder`_
-  `Rol Wheels`_

Bycycle Wheel Warehouse
~~~~~~~~~~~~~~~~~~~~~~~

Bicycle Wheel Warehouse looks like they pull from a subset of commercial
components and have their own in-house line called Pure (hubs) and
Blackset Race (rims). I noticed their address was on my daily bike route
in Huntington Beach…but they don't have a walk in store. I also ran into
mixed satisfaction with customer service (mainly responsiveness) in
reviews and forum discussions (these and others). Though the majority
seem happy with the wheels themselves. I attempted to contact the shop,
but no one answered. Left a message, but received no return call. This
being my first wheel set I'd prefer a bit more hand
holding/accessibility.

My other contention with BWW was a certain level of uncertainty I wasn't
quite comfortable with. There was very vocal support on the `Road Bike
Review`_ forums (whom BWW sponsors). However, it mainly comes from a few
people. There were more than a few anecdotal, positive stories. But a
deep review history of their products does not exist.

None of these would have been deal breakers. BWW's prices are quite good
and they're somewhat local despite the lack of a store front. However,
in combination they put me off as a new buyer. Frankly, if I could name
one thing that was *the* deterrant it would be responsiveness. If they'd
have answered or returned my call within a day or so, they'd have
probably made a sale.

Predator Cycling
~~~~~~~~~~~~~~~~

So, Predator Cycling only deals with high end. They're local (Los
Angeles) and have a walk in shop that I've been fit in. Their main focus
is building custom bikes from the ground up, wheels being part of that.
So I was still tempted. But their wheels started at around 900-1200 a
set which was a bit more than I wanted to pay. Was really impressed with
Aram's customer service and the fit feels quite good. He also said he'd
work with me with any wheels I picked up.

I liked the feel of the shop and Aram seemed very passionate about his
work. When it's time to upgrade my bike, I'll seriously consider
purchasing it from them. No negatives with Predator Cycling whatsoever,
just a bit out of my range.

Wheel Builder
~~~~~~~~~~~~~

Wheel Builder is an outfit located in El Monte, CA. which is semi-local
for me at approximately 30 miles away. They were recommended by Aram at
predator as a solid wheel builder and I have no reason to doubt that
recommendation. The only negative was that they were a bit pricey for
the components being built. Not a deal breaker in and of itself, after
all they *are* custom built wheels. But a few hundred dollars is still a
few hundred dollars.

Main stream options
~~~~~~~~~~~~~~~~~~~

No need to really cover these in detail. Shiimano, DT, WI, etc. They're
solid, they're known, there's plenty of information out there. Most in
the price range I was looking at are factory built. Easton would be one
exception, but I've received so many anecdotal reports of busted spokes,
loose hubs, etc. that I was hesitant to dive in.

Having worked in plants and factories before, there was one thing I was
sure of. When it comes to precision work, a hand built product done by
someone who cares about their work is vastly superior to assembly line
fabrication. So I was leaning toward a hand built wheel by a small shop.

Rol Wheels
~~~~~~~~~~

Rol Wheels is based in Austin, TX. which has a lively and strong cycling
scene. They've been a sponsor of Road Bike Review for quite some time.
They've been in the Road Bike Review "Best of XX Year" list for 4 years
in a row with over 150 reviews non of which are below a 4.5....that
caught my attention A recurring theme amongst reviewers and forum
commenters was the outstandingly, over the top customer service. That
held my attention.

In contrast to BWW whose feedback and users seemed to mainly congregate
around Road Bike Review, I was able to find discussions of Rol Wheels in
many other forums, blogs, and a couple of magazines. `Peloton Magazine`_
(formally testrider.com) also covered the line. The greater presence
could simply be indicative of better marketing. Regardless, it was nice
to have a more diverse set of sources. Finally their prices are quite
good and fell squarely within my price range.

As a final test, I decided to see if the raves about customer service
could hold water. I called up Rol first thing in the morning. Sean, the
owner, picked up on the second ring. He asked my weight and my intended
uses (durability, reliability, dual purpose train/race). He said I was
light enough to ride any of their wheelsets regularly, even the 20/24,
1478g `D"Huez`_. However, the `Volant R/T`_ is their most robust line
weighing in at 1655g with 24/28 spoke configuration. I mentioned that I
wanted something that could take a bit of abuse and not come away like a
taco. To which he replied that if I never irreparably dent the rim,
they'd be happy to repair it for $60 dollars...so a busted rim doesn't
result in a trip to the savings account.

That's the sort of feedback that inspires purchasing confidence. It may
be superficial, but talking to a human being that sounds excited about
their work and generally eager to make you a customer without trying to
upsell is the kind of interaction that wins customers and creates
loyalty.

The Winner
----------

I went with Rol Wheels (could probably tell that already) and I plinked
down for the Volant R/T. I decided as a newbie that cutting 2268 grams
off the waste for free was a cheaper way to lighten my bike than cutting
177 grams for $250 dollars. They're still a lot lighter than the Open
Sports I'm using now.


I'll make an unboxing post when I receive them with first impressions
and, of course, a final impression after I put a few thousand kilometers
on them.

.. _Bicycle Wheel Warehouse: http://www.bicyclewheelwarehouse.com/
.. _Predator Cycling: http://predatorcycling.com/
.. _Wheel Builder: http://www.wheelbuilder.com/
.. _Rol Wheels: http://www.rolwheels.com/
.. _Road Bike Review: http://forums.roadbikereview.com/
.. _Peloton Magazine: http://www.pelotonmagazine.com/
.. _D"Huez: http://www.rolwheels.com/rol_dhuez_wheels.php
.. _Volant R/T: http://www.rolwheels.com/rol_volant_rt_wheels.php
