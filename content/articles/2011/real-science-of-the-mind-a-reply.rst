Real Science of the Mind: A reply
#################################
:date: 2011-01-06 18:11
:category: opinion
:tags: neuroscience, opinion, philosophy, psychology

Tyler Burge wrote an interesting `op-ed`_ that took a pretty hostile
tone toward neuroscience. Starting right off the bat referring to
neuroscience as "neurobabble". I took a few contentions with the fast
and loose critique he provides.I propose that the dichotomy between
explanation and description is false.

For example, given (feel free to offer alternative definitions):


    An explanation is a set of statements constructed to describe a set
    of facts which clarifies the causes, context, and consequences of
    those facts.

This is what neuroscientists are doing when they provide "descriptions"
of various phenomena. Now, often when people point to this distinction
between explanation vs. description, they're referring to some concept
of "purpose". However, the author did not make this clear nor did he
propose any reason why psychology is better equipped or suited toward
providing "purpose" than neuroscience. I could be misinterpreting, but
it would have been very helpful if the distinction was explicitly
provided.

We'll go over the reasons he did enumerate.

First, it provides little insight into psychological phenomena.
---------------------------------------------------------------

By this I assume he means the qualia of the biological state of love.
Without addressing problems with qualia itself, I would question what
advantage psychology has over neuroscience in providing adequate
explanations. None are offered by the author. Further, qualia, as a
phenomena, is framed in such a way that it may be impossible to even
arrive at an adequate explanation that is expressible by language
(Dennet talks about this at length).

Individuals see, know, and want to make love. Brains don’t.
-----------------------------------------------------------

Neuroscience does not make such simplistic claims as "brains are
everything". There is very deep discussion in the field over systems
analysis, or the cumulative effect of multiple contributions to a
particular mental state. DTI research is related to this as it attempts
to study the \*pathways\* that a signal takes rather than focusing on
only the areas of the most significant neural activities. Also, in this
specific case it is not suggested that this one locus of the brain is
where "love" starts and stops. Biologists and neuroscientists are well
aware that we're dealing with a concert of activity ( throughout the
entire body), the cumulative effect of which is our subjective
experience in totality. However, the brain is considered a crucial and
significant domino in the chain without which all other activity would
cease. We also know if you break communication between the body and the
brain, the qualia of "love" is never achieved.

Brain activity is necessary for psychological phenomena, but its relation to them is complex.
---------------------------------------------------------------------------------------------

He does admit/acknowledge this, but again presents no evidence or even
an opinion on \*why\* psychology is in a uniquely superior position to
address this problem. Neuroscientists know we're dealing with a
massively complex system, but so are psychologists.

    Explanations of neural phenomena are not themselves explanations of
    psychological phenomena.

And what is? What is the framework? How does it obscure the discussion?
What makes psychologybabble superior to neurobabble?

See John Cleese’s apt spoof of such reductionism.
-------------------------------------------------

What makes John Cleese's discussion absurd is not the claim made by the
study , but the way he reduces it to an absolute. All science talks of
correlations and probabilities, not absolute certainties. Here are the
actual claims of the God Gene hypothesis:

#. spirituality can be quantified by psychometric measurements
#. the underlying tendency to spirituality is partially heritable
#. part of this heritability can be attributed to the gene VMAT2
#. this gene acts by altering monoamine levels
#. spirituality arises in a population because spiritual individuals are
   favored by natural selection.

Notice the liberal use of terms like "partially". There's no one-to-one
connection between a single gene or a single chemical or a single event
that guarantees spirituality. Further, this theory is on the fringe (for
now). He didn't publish his data in a peer reviewed journal and the
community hasn't digested it yet.Cleese acts like the suggestion is that
a very specific behavior is hardwired by a single, specific gene. If
there's any doubts about this being the claim, I'll let Hamer clarify in
his own words:

    Just because spirituality is partly genetic doesn't mean it is
    hardwired.

I don't think that neuroscience will replace psychology, but there's
very little reason to doubt that they will merge. He uses references to
biology quite a bit, so I'll follow that train of thought. Prior to
modern genetics research, we made great strides in understanding and
formalizing the process of inheritance. The theory of evolution itself
was derived without the aid of modern genetics. However, it also
produced a lot of trash that on the surface seemed very reasonable. An
example would be our assumptions about race, a concept that's been
proven to be nonsensical by genetics and completely unrelated to actual
biological "nearness". But we don't have some sort of " schism" between
"genealogists" that claim genetics is intrinsically incapable of
answering important questions. We just have geneticists. They use a lot
of different tools depending on the problem at hand and one of those is
modern computational genetics.

I see no reason why this won't happen with neuroscience. The
mathematical models for vision and generative linguistics are just one
more tool for the overarching class. Used when most appropriate for
solving a problem or in combination with other tools if their seen to
augment. And this is exactly [ what's
happening](http://www.ncbi.nlm.nih.gov/sites/entrez).

I look forward to the upcoming years as psychology and neuroscience
merge into one discipline. The combination of the two will likely
augment weaknesses and eliminate fallacies in both to produce a better
discipline.

.. _op-ed: http://opinionator.blogs.nytimes.com/2010/12/19/a-real-science-of-mind/
