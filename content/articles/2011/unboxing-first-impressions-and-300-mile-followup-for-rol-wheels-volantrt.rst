Unboxing, first impressions, and 300 mile followup for Rol Wheel's Volant/RT
#################################################################################
:date: 2011-06-06 10:28
:category: reviews
:tags: cycling, fitness, wheels
:slug: rol-wheels-review

For the write up on why I chose Rol Wheels over the many other custom
and commercial options for wheels, see my original post `My experience
as a newbie cyclist buying his first set of non stock wheels.`_.


The Unboxing
------------

.. figure:: /images/2011/06/Rol-Wheels-1.jpg
   :align: center
   :alt: Rol Wheels 1.jpg

Though packed in nothing but cardboard, the wheels were well protected
and secure. It couldn't have withstood a pallet of cinder blocks being
dropped on them or anything, but appeared perfectly suitable for normal
shipping bumps and bangs.

.. figure:: /images/2011/06/Rol-Wheels-2.jpg
   :align: center
   :alt: Rol Wheels 2.jpg

The wheels themselves looked absolutely sexy. Polished with nice looking
decals. A quick ping test on spoke tension gave an equal tone for all
spokes. I didn't put them up on a truing rack right off the bat, but a
visual check on the mounted wheels didn't indicate any warping. In
hindsight, I should have driven them up to the shop if only for a
reference on the 300 mile followup. As others have mentioned on many of
the `Rol Wheels reviews on roadbikereview.com`_, the only negative as
far as construction might be the plastic pokers for the hubs. But, in my
book, this is a minor detail.

The Ride
--------

Well, of course the *real* test of a new wheel set is when they're doing
what their meant to do...rolling! And I couldn't have had a better
impression of the Volants at that task. They spin up well and corner
great. Now, as I mentioned in my original post I'm a newbie so I'm
likely not pushing the absolute limits of my bike. However, I did seem
to notice a difference between the Volants and my stock Mavic Open
Sport's with shimano 105 hubs. They seemed to hold the line a bit better
without flex on tight turns. It could all be in my head or maybe I was
just really trying to push the new wheels and just got more out of them
because of that. Bottom line is, no dissappointment on that front.

The only possible negative to the wheels is the hubs are of the noisy
variety when freewheeling. It doesn't bother me, but if such things do
bother you it's something to take into consideration. Personally, in
some ways I even like it. The only time I freewheel is when I'm slowing
for other riders or pedestrians on bike paths and the audible click
usually lets them know I've rolled up behind them. However, the noise is
low enough to be drowned out by the wind for the most part.

The 300 Mile Followup
---------------------

To make one thing clear off the bat, after 300+ miles of riding I don't
expect a new set of wheels to remain true. . . even if they've been
pre-stressed. You should *always* do a followup check on your new wheels
to make sure they held tru after teh first few weeks of riding. As such,
none of the adjustments I note below in the followup should be taken as
a slight against the quality of Rol Wheels.

After approximately 300 to 400 miles of road time, I brought the wheels
into my local bike coop `Bikerowave`_, free to all `Santa Monica
College`_ students! While there I threw the wheels up on a truing stand.

The rear wheel had become dished toward the side with the shorter spoke
angle. One thing I found odd is that it was *perfectly* dished, by which
I mean they were evenly dished arround the entire wheel. This is
something that the naked eye likely wouldn't catch on a quick spin check
unless you were paying close attention to the spacing between the wheel
and the rear fork near the bottom bracket. This is why I wished I'd
taken the time to bring them to the shop out of the box to see if they
came from the factory like that. If they had, it may be an indication
that one of Sean's truing stands is dished. However, it's not a big deal
to correct which is exactly what I did with a few 1/4 turns around the
circumference of the wheel.

The front wheel was still very, very close to tru. I tightened it up a
bit bringing it to within 1mm which is good enough for government work.

Final Verdict
-------------

So, the final verdict is I'm very happy with the new wheel set. If given
the choice to do over I'd make the same decision. The weight to price to
performance ratio is stellar and when I called with questions Sean, the
owner, was very responsive (picked up on the first ring).

.. _My experience as a newbie cyclist buying his first set of non stock wheels.: /2011/05/my-experience-as-a-newbie-cyclist-buying-his-first-set-of-non-stock-wheels/
.. _Rol Wheels reviews on roadbikereview.com: http://www.roadbikereview.com/mfr/rolwheels/wheelsets/mpl_13414_2490crx.aspx
.. _Bikerowave: http://www.bikerowave.org/
.. _Santa Monica College: http://www.smc.edu
