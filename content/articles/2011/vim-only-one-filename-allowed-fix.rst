Fix for MacVim Syntax Error, Throws "Only one file name allowed"
################################################################
:date: 2011-02-25 11:58
:category: utilities
:tags: programming, vim
:slug: vim-only-one-filename-allowed-fix


I keep a lot of my user installed applications in /Applications/User
Utilities. Keeps some modicum of organization and reduces a bit of
clutter in my /Applications directory. After moving `MacVim`_ to this
directory, I began getting several errors when opening files. They were
all of the same type.

::

    E172: Only one file name allowed

With occurrances on several lines in the runtime/syntax/vim.vim file.
Opening the file, I found syntax like the following

.. code-block:: vim
    
    exe "syn include @vimPerlScript ".s:perlpath

You may notice the perpath variable is where the problem lies. the space
gives the appearance of two arguments instead of a single path. We can
prevent this by using fnameescape For me, these errors occurred on lines
585, 607, and 628.

-  Line 585

   .. code-block:: vim

       exe "syn include @vimPerlScript ".s:perlpath

   becomes

   .. code-block:: vim

       exe "syn include @vimPerlScript ".fnameescape(s:perlpath)

-  Line 607

   .. code-block:: vim

       exe "syn include @vimRubyScript ".s:rubypath

   becomes

   .. code-block:: vim

       exe "syn include @vimRubyScript ".fnameescape(s:rubypath)

-  Line 628

   .. code-block:: vim

       exe "syn include @vimPythonScript ".s:pythonpath

   becomes

   .. code-block:: vim

       exe "syn include @vimPythonScript ".fnameescape(s:pythonpath)

All done.

.. _MacVim: http://code.google.com/p/macvim/
