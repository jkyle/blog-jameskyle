Automatically create django User Profiles as needed.
####################################################
:date: 2011-02-25 11:10
:category: programming
:tags: django, programming, python
:slug: autocreate-django-user-profiles

Ran across a very nice, brief run down of creating user profiles for
django on `Turnkey Linux`_.

I was particularly pleased of this "ism" that creates user profiles on
the fly on an as needed basis. As a bonus, it provides access to the
profiles through a user.profile property. All you have to do is place
the following property declaration at the bottom of your profile's
models.py.

.. code-block:: python

    User.profile = property(lambda u: UserProfile.objects.get_or_create(user=u)[0])

And that's it. Now, when someuser.profile is called, it'll create a
default profile if needed or return that user's profile if it exists.
See the original article for a full User Profile quickstart.

.. _Turnkey Linux: http://www.turnkeylinux.org/blog/django-profile
