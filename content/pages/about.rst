About me
########
:date: 2013-05-25 23:47
:author: James A. Kyle
:slug: about

I'm currently working at the `AT&T Foundry`_ as the primary devopsy kind of
fella. I do a lot of work with and around OpenStack, SDN, the ever buzz worthy
Big Data, puppet, chef, and other automated deployment systems.

Before that I worked about a year in a MRI medical device startup developing
paradigm stimulation and patient distraction software and for a few years with 
grid compute clusters at the UCLA Center for Cognitive Neuroscience with 
`Mark Cohen`_ and `Susan Bookheimer`_. 


.. _`AT&T Foundry`: https://www.foundry.att.com/
.. _`Mark Cohen`: http://faculty.neuroscience.ucla.edu/institution/personnel?personnel_id=45596 
.. _`Susan Bookheimer`: http://faculty.neuroscience.ucla.edu/institution/personnel?personnel_id=9716
