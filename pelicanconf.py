#!/usr/bin/env python
# -*- coding: utf-8 -*- #
from __future__ import unicode_literals
import os

SITEURL = 'http://blog.jameskyle.org'
AUTHOR = u"James A. Kyle"
AUTHOR_EMAIL = u"james@jameskyle.org"
SITENAME = u"miscellaneous"
SITEURL = "http://blog.jameskyle.org"
ARTICLE_URL = "{date:%Y}/{date:%m}/{slug}"
ARTICLE_SAVE_AS = "{date:%Y}/{date:%m}/{slug}/index.html"
TAG_URL = "tag/{name}"
TAG_SAVE_AS = "tag/{name}/index.html"
TIMEZONE = 'America/Los_Angeles'

DEFAULT_LANG = 'en'

DISPLAY_PAGES_ON_MENU = True

STATIC_PATHS = ["images", "source"]

# Feeds
FEED_ALL_ATOM = None
FEED_ALL_RSS = None
FEED_ATOM = 'feeds/atom.xml'
FEED_RSS = 'feeds/rss.xml'
CATEGORY_FEED_ATOM = 'feeds/categories/%s.atom.xml'
TAG_FEED_ATOM = 'feeds/tags/%s.atom.xml'

# Disqus
DISQUS_SITENAME = "blog-jameskyle"

# Twitter
TWITTER_USERNAME = "jameskyle75"

# Blogroll
LINKS = (
    ('Pelican', 'http://docs.notmyidea.org/alexis/pelican/'),
    ('Doug Hellmann', 'http://www.doughellmann.com'),
    ('Openstack', 'http://www.openstack.org/blog/')
)

# Social widget
SOCIAL = (
    ('github', 'http://github.com/jameskyle/'),
    ('twitter', 'https://twitter.com/jameskyle75'),
    ('bitbucket', 'https://bitbucket.org/jkyle'),
    ('google+', 'https://plus.google.com/+jameskyle75'),
    ('linkedin', 'http://www.linkedin.com/in/jamesakyle'),
)
DEFAULT_PAGINATION = 10

# Tag Cloud
TAG_CLOUD_STEPS = 10
TAG_CLOUD_MAX_ITEMS = 100

# github
GITHUB_URL = "http://github.com/jameskyle/"
RELATIVE_URLS = True

THEME = "theme"
MOBILE_CSS_FILE = "mobile.css"
PLUGINS = ['gravatar', 'share_post']
#PLUGINS = ['tipue_search', 'gravatar', 'assets', 'sitemap']

if "PELICAN_PLUGINS" in os.environ:
    PLUGIN_PATHS = os.environ['PELICAN_PLUGINS'].split(',')
else:
    PLUGIN_PATHS = ['/Users/jkyle/Projects/blog/pelican-plugins']

