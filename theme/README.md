# simplegrey #



## Screenshot ##

![screenshot](screenshot.png)


## Credits ##

* Theme freely inspired by [vkvn](https://github.com/vkvn)
* Icons by [Jorge Calvo](http://dribbble.com/shots/1074961-Flat-Icons-EPS), slightly adapted by [Ingrid Hamard](http://ingrid.hamard.free.fr)

