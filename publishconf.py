#!/usr/bin/env python
# -*- coding: utf-8 -*- #

import sys
sys.path.append('.')
from pelicanconf import *

#DELETE_OUTPUT_DIRECTORY = True
DELETE_OUTPUT_DIRECTORY = False

# Following items are often useful when publishing

# Uncomment following line for absolute URLs in production:
RELATIVE_URLS = True

GOOGLE_ANALYTICS = "UA-23876265-2"
